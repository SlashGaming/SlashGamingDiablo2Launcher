﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.localcrcTxt = New System.Windows.Forms.TextBox()
        Me.patchPrgBr = New System.Windows.Forms.ProgressBar()
        Me.servercrcTxt = New System.Windows.Forms.TextBox()
        Me.playBtn = New System.Windows.Forms.Button()
        Me.GroupBoxLaunchFlags = New System.Windows.Forms.GroupBox()
        Me.customargsTxtBox = New System.Windows.Forms.TextBox()
        Me.directtxtChk = New System.Windows.Forms.CheckBox()
        Me.customChk = New System.Windows.Forms.CheckBox()
        Me.aspectChk = New System.Windows.Forms.CheckBox()
        Me.dfxChk = New System.Windows.Forms.CheckBox()
        Me.nsChk = New System.Windows.Forms.CheckBox()
        Me.skipChk = New System.Windows.Forms.CheckBox()
        Me.wChk = New System.Windows.Forms.CheckBox()
        Me.runasChk = New System.Windows.Forms.CheckBox()
        Me.GroupBoxPatchPicker = New System.Windows.Forms.GroupBox()
        Me.ComboBoxPatchPicker = New System.Windows.Forms.ComboBox()
        Me.btnInstallPath = New System.Windows.Forms.Button()
        Me.btnBHPath = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.bhBtn = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.ComboBoxLaunchModePicker = New System.Windows.Forms.ComboBox()
        Me.ComboBoxNumberPicker = New System.Windows.Forms.ComboBox()
        Me.DEP_Only = New System.Windows.Forms.Button()
        Me.DEP_and_XP_SP2 = New System.Windows.Forms.Button()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBoxLaunchFlags.SuspendLayout()
        Me.GroupBoxPatchPicker.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.localcrcTxt)
        Me.GroupBox2.Controls.Add(Me.patchPrgBr)
        Me.GroupBox2.Controls.Add(Me.servercrcTxt)
        Me.GroupBox2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.GroupBox2.Location = New System.Drawing.Point(726, 80)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(171, 86)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "CRC Check"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label4.Location = New System.Drawing.Point(12, 49)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(33, 13)
        Me.Label4.TabIndex = 9
        Me.Label4.Text = "Local"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label3.Location = New System.Drawing.Point(12, 27)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(38, 13)
        Me.Label3.TabIndex = 9
        Me.Label3.Text = "Server"
        '
        'localcrcTxt
        '
        Me.localcrcTxt.Location = New System.Drawing.Point(64, 49)
        Me.localcrcTxt.Name = "localcrcTxt"
        Me.localcrcTxt.ReadOnly = True
        Me.localcrcTxt.Size = New System.Drawing.Size(91, 20)
        Me.localcrcTxt.TabIndex = 0
        '
        'patchPrgBr
        '
        Me.patchPrgBr.Location = New System.Drawing.Point(64, 49)
        Me.patchPrgBr.Name = "patchPrgBr"
        Me.patchPrgBr.Size = New System.Drawing.Size(91, 20)
        Me.patchPrgBr.TabIndex = 2
        Me.patchPrgBr.Visible = False
        '
        'servercrcTxt
        '
        Me.servercrcTxt.Location = New System.Drawing.Point(64, 24)
        Me.servercrcTxt.Name = "servercrcTxt"
        Me.servercrcTxt.ReadOnly = True
        Me.servercrcTxt.Size = New System.Drawing.Size(91, 20)
        Me.servercrcTxt.TabIndex = 3
        Me.servercrcTxt.Text = "Checking..."
        '
        'playBtn
        '
        Me.playBtn.BackColor = System.Drawing.Color.LightGray
        Me.playBtn.Enabled = False
        Me.playBtn.Location = New System.Drawing.Point(726, 465)
        Me.playBtn.Name = "playBtn"
        Me.playBtn.Size = New System.Drawing.Size(127, 47)
        Me.playBtn.TabIndex = 1
        Me.playBtn.Text = "Checking for Update"
        Me.playBtn.UseVisualStyleBackColor = False
        '
        'GroupBoxLaunchFlags
        '
        Me.GroupBoxLaunchFlags.Controls.Add(Me.customargsTxtBox)
        Me.GroupBoxLaunchFlags.Controls.Add(Me.directtxtChk)
        Me.GroupBoxLaunchFlags.Controls.Add(Me.customChk)
        Me.GroupBoxLaunchFlags.Controls.Add(Me.aspectChk)
        Me.GroupBoxLaunchFlags.Controls.Add(Me.dfxChk)
        Me.GroupBoxLaunchFlags.Controls.Add(Me.nsChk)
        Me.GroupBoxLaunchFlags.Controls.Add(Me.skipChk)
        Me.GroupBoxLaunchFlags.Controls.Add(Me.wChk)
        Me.GroupBoxLaunchFlags.ForeColor = System.Drawing.Color.Silver
        Me.GroupBoxLaunchFlags.Location = New System.Drawing.Point(725, 224)
        Me.GroupBoxLaunchFlags.Name = "GroupBoxLaunchFlags"
        Me.GroupBoxLaunchFlags.Size = New System.Drawing.Size(171, 214)
        Me.GroupBoxLaunchFlags.TabIndex = 4
        Me.GroupBoxLaunchFlags.TabStop = False
        Me.GroupBoxLaunchFlags.Text = "Launch Flags"
        '
        'customargsTxtBox
        '
        Me.customargsTxtBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Global.SlashGamingLauncher.My.MySettings.Default, "CustomArguments", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.customargsTxtBox.Location = New System.Drawing.Point(6, 184)
        Me.customargsTxtBox.Name = "customargsTxtBox"
        Me.customargsTxtBox.Size = New System.Drawing.Size(159, 20)
        Me.customargsTxtBox.TabIndex = 7
        Me.customargsTxtBox.Text = Global.SlashGamingLauncher.My.MySettings.Default.CustomArguments
        '
        'directtxtChk
        '
        Me.directtxtChk.AutoSize = True
        Me.directtxtChk.Location = New System.Drawing.Point(6, 138)
        Me.directtxtChk.Name = "directtxtChk"
        Me.directtxtChk.Size = New System.Drawing.Size(72, 17)
        Me.directtxtChk.TabIndex = 6
        Me.directtxtChk.Text = "-direct -txt"
        Me.directtxtChk.UseVisualStyleBackColor = True
        '
        'customChk
        '
        Me.customChk.AutoSize = True
        Me.customChk.Location = New System.Drawing.Point(6, 161)
        Me.customChk.Name = "customChk"
        Me.customChk.Size = New System.Drawing.Size(141, 17)
        Me.customChk.TabIndex = 5
        Me.customChk.Text = "Custom (disables above)"
        Me.customChk.UseVisualStyleBackColor = True
        '
        'aspectChk
        '
        Me.aspectChk.AutoSize = True
        Me.aspectChk.Location = New System.Drawing.Point(6, 115)
        Me.aspectChk.Name = "aspectChk"
        Me.aspectChk.Size = New System.Drawing.Size(83, 17)
        Me.aspectChk.TabIndex = 4
        Me.aspectChk.Text = "-nofixaspect"
        Me.aspectChk.UseVisualStyleBackColor = True
        '
        'dfxChk
        '
        Me.dfxChk.AutoSize = True
        Me.dfxChk.Location = New System.Drawing.Point(6, 91)
        Me.dfxChk.Name = "dfxChk"
        Me.dfxChk.Size = New System.Drawing.Size(143, 17)
        Me.dfxChk.TabIndex = 3
        Me.dfxChk.Text = "-3dfx (glide wrapper only)"
        Me.dfxChk.UseVisualStyleBackColor = True
        '
        'nsChk
        '
        Me.nsChk.AutoSize = True
        Me.nsChk.Location = New System.Drawing.Point(6, 67)
        Me.nsChk.Name = "nsChk"
        Me.nsChk.Size = New System.Drawing.Size(93, 17)
        Me.nsChk.TabIndex = 2
        Me.nsChk.Text = "-ns (no sound)"
        Me.nsChk.UseVisualStyleBackColor = True
        '
        'skipChk
        '
        Me.skipChk.AutoSize = True
        Me.skipChk.Location = New System.Drawing.Point(6, 43)
        Me.skipChk.Name = "skipChk"
        Me.skipChk.Size = New System.Drawing.Size(78, 17)
        Me.skipChk.TabIndex = 1
        Me.skipChk.Text = "-skiptobnet"
        Me.skipChk.UseVisualStyleBackColor = True
        '
        'wChk
        '
        Me.wChk.AutoSize = True
        Me.wChk.Location = New System.Drawing.Point(6, 19)
        Me.wChk.Name = "wChk"
        Me.wChk.Size = New System.Drawing.Size(111, 17)
        Me.wChk.TabIndex = 0
        Me.wChk.Text = "-w (window mode)"
        Me.wChk.UseVisualStyleBackColor = True
        '
        'runasChk
        '
        Me.runasChk.AutoSize = True
        Me.runasChk.ForeColor = System.Drawing.Color.Silver
        Me.runasChk.Location = New System.Drawing.Point(731, 443)
        Me.runasChk.Name = "runasChk"
        Me.runasChk.Size = New System.Drawing.Size(122, 17)
        Me.runasChk.TabIndex = 5
        Me.runasChk.Text = "Run as administrator"
        Me.runasChk.UseVisualStyleBackColor = True
        '
        'GroupBoxPatchPicker
        '
        Me.GroupBoxPatchPicker.BackColor = System.Drawing.Color.Black
        Me.GroupBoxPatchPicker.Controls.Add(Me.ComboBoxPatchPicker)
        Me.GroupBoxPatchPicker.ForeColor = System.Drawing.Color.Silver
        Me.GroupBoxPatchPicker.Location = New System.Drawing.Point(726, 170)
        Me.GroupBoxPatchPicker.Name = "GroupBoxPatchPicker"
        Me.GroupBoxPatchPicker.Size = New System.Drawing.Size(172, 49)
        Me.GroupBoxPatchPicker.TabIndex = 7
        Me.GroupBoxPatchPicker.TabStop = False
        Me.GroupBoxPatchPicker.Text = "Patch and Gateway"
        '
        'ComboBoxPatchPicker
        '
        Me.ComboBoxPatchPicker.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxPatchPicker.FormattingEnabled = True
        Me.ComboBoxPatchPicker.Items.AddRange(New Object() {"SlashDiablo", "SlashDiablo Resurgence", "Battle.net"})
        Me.ComboBoxPatchPicker.Location = New System.Drawing.Point(7, 19)
        Me.ComboBoxPatchPicker.Name = "ComboBoxPatchPicker"
        Me.ComboBoxPatchPicker.Size = New System.Drawing.Size(159, 21)
        Me.ComboBoxPatchPicker.TabIndex = 6
        Me.ComboBoxPatchPicker.Text = Global.SlashGamingLauncher.My.MySettings.Default.SelectedMod
        '
        'btnInstallPath
        '
        Me.btnInstallPath.BackColor = System.Drawing.Color.LightGray
        Me.btnInstallPath.Location = New System.Drawing.Point(787, 21)
        Me.btnInstallPath.Name = "btnInstallPath"
        Me.btnInstallPath.Size = New System.Drawing.Size(107, 23)
        Me.btnInstallPath.TabIndex = 8
        Me.btnInstallPath.Text = "Set Directory"
        Me.btnInstallPath.UseVisualStyleBackColor = False
        '
        'btnBHPath
        '
        Me.btnBHPath.BackColor = System.Drawing.Color.LightGray
        Me.btnBHPath.Location = New System.Drawing.Point(787, 50)
        Me.btnBHPath.Name = "btnBHPath"
        Me.btnBHPath.Size = New System.Drawing.Size(107, 23)
        Me.btnBHPath.TabIndex = 8
        Me.btnBHPath.Text = "Set Directory"
        Me.btnBHPath.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label1.Location = New System.Drawing.Point(735, 26)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(37, 13)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "Diablo"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label2.Location = New System.Drawing.Point(735, 55)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(22, 13)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "BH"
        '
        'bhBtn
        '
        Me.bhBtn.BackColor = System.Drawing.Color.LightGray
        Me.bhBtn.Location = New System.Drawing.Point(859, 465)
        Me.bhBtn.Name = "bhBtn"
        Me.bhBtn.Size = New System.Drawing.Size(39, 47)
        Me.bhBtn.TabIndex = 1
        Me.bhBtn.Text = "BH"
        Me.bhBtn.UseVisualStyleBackColor = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Location = New System.Drawing.Point(0, 2)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(719, 519)
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'ComboBoxLaunchModePicker
        '
        Me.ComboBoxLaunchModePicker.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxLaunchModePicker.FormattingEnabled = True
        Me.ComboBoxLaunchModePicker.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.ComboBoxLaunchModePicker.Items.AddRange(New Object() {"Game.exe", "Diablo II.exe"})
        Me.ComboBoxLaunchModePicker.Location = New System.Drawing.Point(598, 428)
        Me.ComboBoxLaunchModePicker.Name = "ComboBoxLaunchModePicker"
        Me.ComboBoxLaunchModePicker.Size = New System.Drawing.Size(121, 21)
        Me.ComboBoxLaunchModePicker.TabIndex = 11
        Me.ComboBoxLaunchModePicker.Text = Global.SlashGamingLauncher.My.MySettings.Default.LaunchMode
        '
        'ComboBoxNumberPicker
        '
        Me.ComboBoxNumberPicker.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxNumberPicker.FormattingEnabled = True
        Me.ComboBoxNumberPicker.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.ComboBoxNumberPicker.Items.AddRange(New Object() {"1", "2", "3", "4"})
        Me.ComboBoxNumberPicker.Location = New System.Drawing.Point(599, 479)
        Me.ComboBoxNumberPicker.Name = "ComboBoxNumberPicker"
        Me.ComboBoxNumberPicker.Size = New System.Drawing.Size(121, 21)
        Me.ComboBoxNumberPicker.TabIndex = 10
        Me.ComboBoxNumberPicker.Text = Global.SlashGamingLauncher.My.MySettings.Default.NumberOfWindows
        '
        'DEP_Only
        '
        Me.DEP_Only.BackColor = System.Drawing.Color.LightGray
        Me.DEP_Only.Location = New System.Drawing.Point(12, 463)
        Me.DEP_Only.Name = "DEP_Only"
        Me.DEP_Only.Size = New System.Drawing.Size(111, 47)
        Me.DEP_Only.TabIndex = 12
        Me.DEP_Only.Text = "DEP exception for Diablo II.exe"
        Me.DEP_Only.UseVisualStyleBackColor = False
        '
        'DEP_and_XP_SP2
        '
        Me.DEP_and_XP_SP2.BackColor = System.Drawing.Color.LightGray
        Me.DEP_and_XP_SP2.Location = New System.Drawing.Point(129, 463)
        Me.DEP_and_XP_SP2.Name = "DEP_and_XP_SP2"
        Me.DEP_and_XP_SP2.Size = New System.Drawing.Size(111, 47)
        Me.DEP_and_XP_SP2.TabIndex = 13
        Me.DEP_and_XP_SP2.Text = "DEP + XP SP2 compatibility for Diablo II.exe"
        Me.DEP_and_XP_SP2.UseVisualStyleBackColor = False
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(904, 522)
        Me.Controls.Add(Me.DEP_and_XP_SP2)
        Me.Controls.Add(Me.DEP_Only)
        Me.Controls.Add(Me.ComboBoxLaunchModePicker)
        Me.Controls.Add(Me.ComboBoxNumberPicker)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnBHPath)
        Me.Controls.Add(Me.runasChk)
        Me.Controls.Add(Me.btnInstallPath)
        Me.Controls.Add(Me.GroupBoxPatchPicker)
        Me.Controls.Add(Me.GroupBoxLaunchFlags)
        Me.Controls.Add(Me.bhBtn)
        Me.Controls.Add(Me.playBtn)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.PictureBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "SlashGaming Launcher"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBoxLaunchFlags.ResumeLayout(False)
        Me.GroupBoxLaunchFlags.PerformLayout()
        Me.GroupBoxPatchPicker.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents servercrcTxt As System.Windows.Forms.TextBox
    Friend WithEvents playBtn As System.Windows.Forms.Button
    Friend WithEvents GroupBoxLaunchFlags As System.Windows.Forms.GroupBox
    Friend WithEvents aspectChk As System.Windows.Forms.CheckBox
    Friend WithEvents dfxChk As System.Windows.Forms.CheckBox
    Friend WithEvents nsChk As System.Windows.Forms.CheckBox
    Friend WithEvents skipChk As System.Windows.Forms.CheckBox
    Friend WithEvents wChk As System.Windows.Forms.CheckBox
    Friend WithEvents runasChk As System.Windows.Forms.CheckBox
    Friend WithEvents ComboBoxPatchPicker As ComboBox
    Friend WithEvents GroupBoxPatchPicker As GroupBox
    Friend WithEvents btnInstallPath As Button
    Friend WithEvents directtxtChk As CheckBox
    Friend WithEvents localcrcTxt As TextBox
    Friend WithEvents patchPrgBr As ProgressBar
    Friend WithEvents btnBHPath As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents customargsTxtBox As TextBox
    Friend WithEvents customChk As CheckBox
    Friend WithEvents bhBtn As Button
    Public WithEvents ComboBoxNumberPicker As ComboBox
    Public WithEvents ComboBoxLaunchModePicker As ComboBox
    Friend WithEvents DEP_Only As Button
    Friend WithEvents DEP_and_XP_SP2 As Button
End Class
