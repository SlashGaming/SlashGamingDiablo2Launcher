﻿Imports System.IO
Imports System.Net
'Imports System.Threading '@magic used to allow launching multiple game instances, remove once a better method is found

Public Class Form1

    Dim updateAvailable As Boolean = False
    Dim installPath As String
    Dim BHPath As String
    'new variables to replace the registry ones - confirmed stored inside the launcher
    Dim saveddirecttxt As String
    Dim savedw As String
    Dim savedskip As String
    Dim savedns As String
    Dim saveddfx As String
    Dim savedAspect As String
    Dim savedcustom As String 'There is another CustomArguments in the settings that doesn't seem used for anything
    Dim savedrunAs As String
    Public Function GetCRC32(ByVal sFileName As String) As String
        Try
            Dim FS As FileStream = New FileStream(sFileName, FileMode.Open, FileAccess.Read, FileShare.Read, 8192)
            Dim CRC32Result As Integer = &HFFFFFFFF
            Dim Buffer(4096) As Byte
            Dim ReadSize As Integer = 4096
            Dim Count As Integer = FS.Read(Buffer, 0, ReadSize)
            Dim CRC32Table(256) As Integer
            Dim DWPolynomial As Integer = &HEDB88320
            Dim DWCRC As Integer
            Dim i As Integer, j As Integer, n As Integer
            '@magic this loads from settings now
            'ComboBoxNumberPicker.Text = 1

            'Create CRC32 Table
            For i = 0 To 255
                DWCRC = i
                For j = 8 To 1 Step -1
                    If (DWCRC And 1) Then
                        DWCRC = ((DWCRC And &HFFFFFFFE) \ 2&) And &H7FFFFFFF
                        DWCRC = DWCRC Xor DWPolynomial
                    Else
                        DWCRC = ((DWCRC And &HFFFFFFFE) \ 2&) And &H7FFFFFFF
                    End If
                Next j
                CRC32Table(i) = DWCRC
            Next i

            'Calcualting CRC32 Hash
            Do While (Count > 0)
                For i = 0 To Count - 1
                    n = (CRC32Result And &HFF) Xor Buffer(i)
                    CRC32Result = ((CRC32Result And &HFFFFFF00) \ &H100) And &HFFFFFF
                    CRC32Result = CRC32Result Xor CRC32Table(n)
                Next i
                Count = FS.Read(Buffer, 0, ReadSize)
            Loop
            FS.Close()
            Return Hex(Not (CRC32Result))
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        If Not My.Settings.savedwChk Is Nothing Then '@magic ??? check if the settings variables are undefined, why?
            'If Not My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\D2ResurgenceLauncher", "wChk", Nothing) Is Nothing Then

            'Dim savedwChk = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\D2ResurgenceLauncher", "wChk", Nothing)
            'Dim savedskipChk = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\D2ResurgenceLauncher", "skipChk", Nothing)
            'Dim savednsChk = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\D2ResurgenceLauncher", "nsChk", Nothing)
            'Dim saveddfxChk = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\D2ResurgenceLauncher", "dfxChk", Nothing)
            'Dim savedaspectChk = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\D2ResurgenceLauncher", "aspectChk", Nothing)
            'Dim savedrunasChk = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\D2ResurgenceLauncher", "runasChk", Nothing)
            'Dim saveddirecttxtChk = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\D2ResurgenceLauncher", "directtxtChk", Nothing)
            'Dim savedcustomChk = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\D2ResurgenceLauncher", "customChk", Nothing)

            'To delete as the values are stored and read directly

            'Dim savedwChk = wChk
            'Dim savedskipChk = skipChk
            'Dim savednsChk = dnsChk
            'Dim saveddfxChk = dfxChk
            'Dim savedaspectChk = aspectChk
            'Dim savedrunasChk = runasChk
            'Dim saveddirecttxtChk = directtxtChk
            'Dim savedcustomChk = customChk


            If My.Settings.savedwChk = "True" Then
                wChk.Checked = True
            End If

            If My.Settings.savedskipChk = "True" Then
                skipChk.Checked = True
            End If

            If My.Settings.savednsChk = "True" Then
                nsChk.Checked = True
            End If

            If My.Settings.saveddfxChk = "True" Then
                dfxChk.Checked = True
            End If

            If My.Settings.savedaspectChk = "True" Then
                aspectChk.Checked = True
            End If

            If My.Settings.saveddirecttxtChk = "True" Then
                directtxtChk.Checked = True
            End If

            If My.Settings.savedcustomChk = "True" Then
                customChk.Checked = True
            End If


            If My.Settings.savedrunasChk = "True" Then
                runasChk.Checked = True
            End If

            PictureBox1.Image = My.Resources.ResourceManager.GetObject(Path.Combine(ComboBoxPatchPicker.Text))

        End If

        If My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\Battle.net\Configuration", "Diablo II Battle.net gateways", Nothing) Is Nothing Then
            'Dim defaultGatewayValues() As String = {"1009", "01", "uswest.battle.net", "8", "U.S.West", "useast.battle.net", "6", "U.S.East", "asia.battle.net", "-9", "Asia", "europe.battle.net", "-1", "Europe"}
            My.Computer.Registry.CurrentUser.DeleteSubKey("Software\Battle.net\Configuration")
            '@magic D2ResurgenceLauncherGatewayValues is a horrible name for the gateway array, changed to just GatewayValues
            Dim GatewayValues() As String = {"1009", "05", "uswest.battle.net", "8", "U.S.West", "useast.battle.net", "6", "U.S.East", "asia.battle.net", "-9", "Asia", "europe.battle.net", "-1", "Europe", "8", "evnt.slashdiablo.net", "SlashDiablo Resurgence", "8", "play.slashdiablo.net", "SlashDiablo"}
            My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Battle.net\Configuration", "Diablo II Battle.net gateways", GatewayValues)
        End If

        'If Not My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\Blizzard Entertainment\Diablo II", "BNETIP", Nothing) Is Nothing Then

        '    Dim currentGateway = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\Blizzard Entertainment\Diablo II", "BNETIP", Nothing)

        '    If Not currentGateway = "evnt.slashdiablo.net" Then
        '        setgatewayBtn.Enabled = True
        '        setgatewayBtn.Text = "Set Gateway"
        '    Else
        '        setgatewayBtn.Enabled = True
        '        setgatewayBtn.Text = "Remove Gateway"
        '    End If

        'Else

        '    setgatewayBtn.Enabled = True
        '    setgatewayBtn.Text = "Set Gateway"

        'End If

        If Not My.Settings.InstallPath Is Nothing Then
            installPath = My.Settings.InstallPath
        ElseIf My.Settings.InstallPath = "" And (Not My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\Blizzard Entertainment\Diablo II", "InstallPath", Nothing) Is Nothing) Then
            My.Settings.InstallPath = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\Blizzard Entertainment\Diablo II", "InstallPath", Nothing)
            My.Settings.InstallPath = My.Settings.InstallPath.TrimEnd("\") & "\"
            installPath = My.Settings.InstallPath
        Else
            MsgBox("No install path found in registry, please define one.") '@magic change to settings maybe since it's not stored in registry now
            SetInstallPath()
        End If

        If Not My.Settings.BHPath Is Nothing Then
            BHPath = My.Settings.BHPath
        End If

        'installPath = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\Blizzard Entertainment\Diablo II", "InstallPath", Nothing)

        'installPath = installPath.TrimEnd("\") & "\"



        If System.IO.File.Exists(installPath & "Patch_D2_original.mpq.bak") = True Then
            My.Settings.Diablo2NormalPatchCRC = GetCRC32(installPath & "Patch_D2_original.mpq.bak")
        Else
            My.Settings.Diablo2NormalPatchCRC = ""
        End If

        If System.IO.File.Exists(installPath & "Patch_D2_d2resurgence.mpq.bak") = True Then
            My.Settings.Diablo2ResurgencePatchCRC = GetCRC32(installPath & "Patch_D2_d2resurgence.mpq.bak")
        Else
            My.Settings.Diablo2ResurgencePatchCRC = ""
        End If

        Try
            Dim address As String = "https://gitlab.com/Diablo2ResurgenceDev/diablo2resurgence/raw/master/Diablo2ResurgencePatchNewestCRC"
            Dim client As WebClient = New WebClient()
            Dim reader As StreamReader = New StreamReader(client.OpenRead(address))
            My.Settings.Diablo2ResurgencePatchNewestCRC = reader.ReadToEnd
            servercrcTxt.Text = My.Settings.Diablo2ResurgencePatchNewestCRC
        Catch ex As Exception
            MsgBox("Could not connect to internet" & vbCrLf & ex.Message)
        End Try

        If System.IO.File.Exists(installPath & "Game.exe") = False Then
            If MsgBox("Game.exe not found in specified directory. Please set a correct path." & vbCrLf & vbCrLf & "NOTE: If you do not want to set a directory at this time and exit the launcher, hit cancel.", MsgBoxStyle.OkCancel) = DialogResult.OK Then
                SetInstallPath()
            Else
                ' Quit the application
                End
            End If
        End If

        Dim d2version = FileVersionInfo.GetVersionInfo(installPath & "Game.exe").FileVersion
        If Not d2version = "1, 0, 13, 60" Then
            If Not d2version = "1, 0, 13, 64" Then
                Dim updatedialog As New UpdateD2()

                If updatedialog.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK Then

                    updatedialog.Dispose()

                    Dim gameproc() As Process
                    Dim updatecomplete As Boolean = False

                    While updatecomplete = False
                        gameproc = Process.GetProcessesByName("Diablo II")
                        If gameproc.Count > 0 Then
                            gameproc(0).Kill()
                            updatecomplete = True
                        End If
                    End While

                Else

                    End

                End If
            End If
        End If

        localcrcTxt.Text = My.Settings.Diablo2ResurgencePatchCRC

        If Me.ComboBoxPatchPicker.Text <> "SlashDiablo Resurgence" Then
            playBtn.Enabled = True '@magic is this fixed right?
            playBtn.Text = "Play"
            updateAvailable = False
        Else
            If My.Settings.Diablo2ResurgencePatchNewestCRC = My.Settings.Diablo2ResurgencePatchCRC Then
                playBtn.Enabled = True
                playBtn.Text = "PLAY"
            Else
                If Me.ComboBoxPatchPicker.Text = "SlashDiablo Resurgence" Then
                    playBtn.Enabled = True '@magic is this fixed right?
                    playBtn.Text = "Update Available"
                    updateAvailable = True
                End If
            End If
        End If

        'Else
        '    playBtn.Enabled = False
        '    ComboBoxPatchPicker.Enabled = False
        '    playBtn.Text = "No D2 Installation Found. Check the install path."

        'End If

    End Sub

    Private Sub SetInstallPath()
        Dim FolderBrowserDialog1 As New FolderBrowserDialog

        If FolderBrowserDialog1.ShowDialog() = DialogResult.OK Then
            My.Settings.InstallPath = FolderBrowserDialog1.SelectedPath
            My.Settings.InstallPath = My.Settings.InstallPath.TrimEnd("\") & "\"
            installPath = My.Settings.InstallPath
        End If

        ' DEBUG: MsgBox("Install path set to: " & My.Settings.InstallPath)
        My.Settings.Save()
        My.Settings.Reload()
        Application.Restart()
    End Sub

    ' leaving this out because it's buggy with files from gitlab (gitlab doesn't return total filesize so this doesn't work)
    ' TODO: figure out what to do with this
    'Private Sub patchclient_ProgressChanged(ByVal sender As Object, ByVal e As DownloadProgressChangedEventArgs)
    '    Dim bytesIn As Double = Double.Parse(e.BytesReceived.ToString())
    '    Dim totalBytes As Double = Double.Parse(e.TotalBytesToReceive.ToString())
    '    Dim percentage As Double = bytesIn / totalBytes * 100
    '    MsgBox("bytes in:" & bytesIn & "\%: " & e.ProgressPercentage)
    '    If Int32.Parse(Math.Truncate(percentage)) > 100 Then
    '        patchPrgBr.Value = 100
    '    ElseIf Int32.Parse(Math.Truncate(percentage)) < 0 Then
    '        patchPrgBr.Value = 0
    '    Else
    '        patchPrgBr.Value = (Int32.Parse(Math.Truncate(percentage).ToString()))
    '    End If


    'End Sub

    Private Sub patchclient_DownloadCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.AsyncCompletedEventArgs)

        patchPrgBr.Visible = False
        localcrcTxt.Visible = True

        Dim localCRC = GetCRC32(installPath & "Patch_D2_d2resurgence.mpq.bak")

        My.Settings.Diablo2ResurgencePatchCRC = localCRC
        localcrcTxt.Text = localCRC

        If My.Settings.Diablo2ResurgencePatchNewestCRC = My.Settings.Diablo2ResurgencePatchCRC And Me.ComboBoxPatchPicker.Text = "SlashDiablo Resurgence" Then
            My.Computer.FileSystem.CopyFile(installPath & "Patch_D2_d2resurgence.mpq.bak", installPath & "Patch_D2.mpq", overwrite:=True)

            updateAvailable = False
            playBtn.Enabled = True
            playBtn.Text = "PLAY"
        ElseIf Me.ComboBoxPatchPicker.Text = "SlashDiablo" Or Me.ComboBoxPatchPicker.Text = "Battle.net" Then
            My.Computer.FileSystem.CopyFile(installPath & "Patch_D2_original.mpq.bak", installPath & "Patch_D2.mpq", overwrite:=True)

            updateAvailable = False
            playBtn.Enabled = True
            playBtn.Text = "PLAY"
        Else

            playBtn.Enabled = False
            playBtn.Text = "Patch Failed. Restart launcher."

        End If

    End Sub
    Sub LaunchGame(LaunchMode As String, NumberOfInstances As String)
        Dim d2 As New ProcessStartInfo
        d2.FileName = installPath & "Diablo II.exe"


        '@magic Remove this part since it's moved in the checkboxes
        'Dim d2d As New ProcessStartInfo
        'd2d.FileName = installPath & "Game.exe"

        'Dim d2s As New ProcessStartInfo
        'd2s.FileName = installPath & "Diabo II.exe"

        'Dim hdd2 As New ProcessStartInfo
        'hdd2.FileName = installPath & "HDD2.exe"
        '-----------------------------------------------

        ' TODO: test if not setting this fixed the bug
        'd2.Arguments = ""

        'If directtxtChk.Checked = True Then
        '    d2.Arguments = If(IsNothing(d2.Arguments), "", d2.Arguments) & "-direct -txt "
        '    My.Settings.saveddirecttxtChk = "True"
        'Else
        '    My.Settings.saveddirecttxtChk = "False"
        'End If

        If directtxtChk.Checked = True Then
            d2.Arguments = If(IsNothing(d2.Arguments), "", d2.Arguments) & "-direct -txt "
            My.Settings.saveddirecttxtChk = "True"
        Else
            My.Settings.saveddirecttxtChk = "False"
        End If

        If wChk.Checked = True Then
            d2.Arguments = If(IsNothing(d2.Arguments), "", d2.Arguments) & "-w "
            My.Settings.savedwChk = "True"
        Else
            My.Settings.savedwChk = "False"
        End If

        If skipChk.Checked = True Then
            d2.Arguments = If(IsNothing(d2.Arguments), "", d2.Arguments) & "-skiptobnet "
            My.Settings.savedskipChk = "True"
        Else
            My.Settings.savedskipChk = "False"
        End If

        If nsChk.Checked = True Then
            d2.Arguments = If(IsNothing(d2.Arguments), "", d2.Arguments) & "-ns "
            My.Settings.savednsChk = "True"
        Else
            My.Settings.savednsChk = "False"
        End If

        If dfxChk.Checked = True Then
            d2.Arguments = If(IsNothing(d2.Arguments), "", d2.Arguments) & "-3dfx "
            My.Settings.saveddfxChk = "True"
        Else
            My.Settings.saveddfxChk = "False"
        End If

        If aspectChk.Checked = True Then
            d2.Arguments = If(IsNothing(d2.Arguments), "", d2.Arguments) & "-nofixaspect "
            My.Settings.savedaspectChk = "True"
        Else
            My.Settings.savedaspectChk = "False"
        End If

        If customChk.Checked = True Then
            d2.Arguments = If(IsNothing(d2.Arguments), "", d2.Arguments) & "-nofixaspect "
            My.Settings.savedcustomChk = "True"
        Else
            My.Settings.savedcustomChk = "False"
        End If

        If runasChk.Checked = True Then
            d2.Verb = "runas"
            My.Settings.savedrunasChk = "True"
        Else
            My.Settings.savedrunasChk = "False"
        End If

        If customChk.Checked = True Then
            d2.Arguments = If(IsNothing(d2.Arguments), "", d2.Arguments) & "-nofixaspect "
            My.Settings.savedcustomChk = "True"
        Else
            My.Settings.savedcustomChk = "False"
        End If

        If runasChk.Checked = True Then
            d2.Verb = "runas"
            My.Settings.savedrunasChk = "True"
        Else
            My.Settings.savedrunasChk = "False"
        End If

        If customChk.Checked = True Then
            d2.Arguments = customChk.Text
        End If

        My.Settings.LaunchMode = ComboBoxLaunchModePicker.Text
        My.Settings.NumberOfWindows = ComboBoxNumberPicker.Text

        My.Settings.Save()
        d2.WorkingDirectory = installPath ' set working directory so -direct -txt can function properly (this is like "start in" from a shortcut)

        Dim proc As Process

        'If ComboBoxLaunchModePicker.Text = "HDD2.exe" Then
        '    d2.FileName = installPath & "HDD2.exe"
        'Else
        If ComboBoxLaunchModePicker.Text = "Game.exe" Then
            d2.FileName = installPath & "Game.exe"
        Else
            d2.FileName = installPath & "Diablo II.exe"
        End If
        'End If

        If ComboBoxNumberPicker.Text = "" Then 'vbNull
            ComboBoxNumberPicker.Text = 1
        End If

        For index As Integer = 1 To ComboBoxNumberPicker.Text
            proc = Process.Start(d2)
            System.Threading.Thread.Sleep(750) '@magic Neat, looks like you can call that without importing the entire System.Threading
        Next
        ' Exit the program 
        'End ' don't exit since we can leave it open/minimize for multiple sessions
    End Sub


    Private Sub playBtn_Click(sender As Object, e As EventArgs) Handles playBtn.Click

        If ComboBoxPatchPicker.Text = "" Then
            MsgBox("Please select a patch first and then click play.")
            Exit Sub
        End If

        If updateAvailable = True Then

            localcrcTxt.Visible = False
            patchPrgBr.Visible = True

            If System.IO.File.Exists(installPath & "Patch_D2_d2resurgence.mpq.bak") = True Then

                System.IO.File.Delete(installPath & "Patch_D2_d2resurgence.mpq.bak")

            End If

            If System.IO.File.Exists(installPath & "Patch_D2_original.mpq.bak") = False Then
                My.Computer.FileSystem.RenameFile(installPath & "Patch_D2.mpq", "Patch_D2_original.mpq.bak")
            End If

            Dim patchclient As WebClient = New WebClient

            ' AddHandler patchclient.DownloadProgressChanged, AddressOf patchclient_ProgressChanged
            'TODO: figure out what to do with this (sub does not work properly due to how gitlab provides file data, see sub declaration)

            AddHandler patchclient.DownloadFileCompleted, AddressOf patchclient_DownloadCompleted

            patchclient.DownloadFileAsync(New Uri("https://gitlab.com/Diablo2ResurgenceDev/diablo2resurgence/raw/master/Patch_D2_d2resurgence.mpq.bak"), installPath & "Patch_D2_d2resurgence.mpq.bak")

            playBtn.Text = "Update in Progress"

            playBtn.Enabled = False


        Else

            LaunchGame(ComboBoxLaunchModePicker.Text, ComboBoxNumberPicker.Text)
            'Launches the game in the selected mode with that many windows

        End If

    End Sub

    Private Sub ComboBoxPatchPicker_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBoxPatchPicker.SelectedIndexChanged
        ' Don't do anything when the dropdown loads for the first time and the application sets the value of the dropdown based on the last saved setting
        '@magic this is for dynamic background based on gateway(change)

        If Me.ComboBoxPatchPicker.Text <> "SlashDiablo Resurgence" Then
            playBtn.Enabled = True '@magic is this fixed right?
            playBtn.Text = "Play"
            updateAvailable = False
        Else
            If My.Settings.Diablo2ResurgencePatchNewestCRC = My.Settings.Diablo2ResurgencePatchCRC Then
                playBtn.Enabled = True
                playBtn.Text = "PLAY"
            Else
                If Me.ComboBoxPatchPicker.Text = "SlashDiablo Resurgence" Then
                    playBtn.Enabled = True '@magic is this fixed right?
                    playBtn.Text = "Update Available"
                    updateAvailable = True
                End If
            End If
        End If

        If ComboBoxPatchPicker.Text = My.Settings.SelectedMod Then
            PictureBox1.Image = My.Resources.ResourceManager.GetObject(Path.Combine(ComboBoxPatchPicker.Text))
            Exit Sub
        End If

        ' Save the setting
        My.Settings.SelectedMod = ComboBoxPatchPicker.Text
        '@magic this is for dynamic background based on gateway(load)
        PictureBox1.Image = My.Resources.ResourceManager.GetObject(Path.Combine(ComboBoxPatchPicker.Text))
        My.Settings.Save()

        ' Set the gateway
        SetGateway(ComboBoxPatchPicker.Text)

        ' Run the patch check 
        SetPatch(ComboBoxPatchPicker.Text)
    End Sub

    Private Sub SetPatch(TargetPatch As String)
        Select Case TargetPatch
            Case "Battle.net", "SlashDiablo"
                ' Check if the file exists
                If System.IO.File.Exists(installPath & "Patch_D2_original.mpq.bak") = False Then
                    MsgBox("Error: Original diablo 2 backup patch file not found. Please wait while the program downloads an original MPQ file.")
                    ' download the patch if it's not found
                    Dim patchclient As WebClient = New WebClient
                    'AddHandler patchclient.DownloadProgressChanged, AddressOf patchclient_ProgressChanged
                    ' todo: figure out what to do with this
                    AddHandler patchclient.DownloadFileCompleted, AddressOf patchclient_DownloadCompleted
                    patchclient.DownloadFileAsync(New Uri("https://gitlab.com/Diablo2ResurgenceDev/diablo2resurgence/raw/master/Patch_D2_original.mpq.bak"), installPath & "Patch_D2_original.mpq.bak")

                    playBtn.Text = "Update in Progress"

                    playBtn.Enabled = False
                End If

                'My.Computer.FileSystem.RenameFile(installPath & "patch_d2.mpq", "patch_d2.mpq.bak")
                My.Computer.FileSystem.CopyFile(installPath & "Patch_D2_original.mpq.bak", installPath & "Patch_D2.mpq", overwrite:=True)

            Case "SlashDiablo Resurgence"
                ' Check if the file exists
                If System.IO.File.Exists(installPath & "Patch_D2_d2resurgence.mpq.bak") = False Then
                    ' download the patch if it's not found
                    Dim patchclient As WebClient = New WebClient
                    'AddHandler patchclient.DownloadProgressChanged, AddressOf patchclient_ProgressChanged
                    ' todo: figure out what to do with this
                    AddHandler patchclient.DownloadFileCompleted, AddressOf patchclient_DownloadCompleted
                    patchclient.DownloadFileAsync(New Uri("https://gitlab.com/Diablo2ResurgenceDev/diablo2resurgence/raw/master/Patch_D2_d2resurgence.mpq.bak"), installPath & "Patch_D2_d2resurgence.mpq.bak")

                    playBtn.Text = "Update in Progress"

                    playBtn.Enabled = False
                End If

                'My.Computer.FileSystem.RenameFile(installPath & "patch_d2.mpq", "patch_d2.mpq.bak")
                ' set the current patch_d2 file to the resurgence patch
                My.Computer.FileSystem.CopyFile(installPath & "Patch_D2_d2resurgence.mpq.bak", installPath & "Patch_D2.mpq", overwrite:=True)


            Case Else

        End Select
    End Sub

    Private Sub SetGateway(SelectedPatch As String)
        Select Case SelectedPatch
            Case "Battle.net"
                My.Computer.Registry.CurrentUser.DeleteSubKey("Software\Battle.net\Configuration")

                Dim defaultGatewayValues() As String = {"1009", "01", "uswest.battle.net", "8", "U.S.West", "useast.battle.net", "6", "U.S.East", "asia.battle.net", "-9", "Asia", "europe.battle.net", "-1", "Europe"}
                My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Battle.net\Configuration", "Diablo II Battle.net gateways", defaultGatewayValues)
                My.Computer.Registry.CurrentUser.OpenSubKey("Software\Blizzard Entertainment\Diablo II", True).DeleteValue("BNETIP")

            Case "SlashDiablo"
                My.Computer.Registry.CurrentUser.DeleteSubKey("Software\Battle.net\Configuration")
                Dim GatewayValues() As String = {"1009", "05", "uswest.battle.net", "8", "U.S.West", "useast.battle.net", "6", "U.S.East", "asia.battle.net", "-9", "Asia", "europe.battle.net", "-1", "Europe", "8", "play.slashdiablo.net", "SlashDiablo"}
                My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Battle.net\Configuration", "Diablo II Battle.net gateways", GatewayValues)

                My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Blizzard Entertainment\Diablo II", "BNETIP", "play.slashdiablo.net")
                My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Blizzard Entertainment\Diablo II", "Preferred Realm", "SlashDiablo")

            Case "SlashDiablo Resurgence"
                My.Computer.Registry.CurrentUser.DeleteSubKey("Software\Battle.net\Configuration")
                Dim GatewayValues() As String = {"1009", "05", "uswest.battle.net", "8", "U.S.West", "useast.battle.net", "6", "U.S.East", "asia.battle.net", "-9", "Asia", "europe.battle.net", "-1", "Europe", "8", "evnt.slashdiablo.net", "Resurgence"}
                My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Battle.net\Configuration", "Diablo II Battle.net gateways", GatewayValues)

                My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Blizzard Entertainment\Diablo II", "BNETIP", "evnt.slashdiablo.net")
                My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Blizzard Entertainment\Diablo II", "Preferred Realm", "Resurgence")
            Case Else

        End Select
    End Sub

    Private Sub btnInstallPath_Click(sender As Object, e As EventArgs) Handles btnInstallPath.Click
        If MsgBox("Current install path:" & vbCrLf & My.Settings.InstallPath & vbCrLf & vbCrLf & "Are you sure you want to change it?", MessageBoxButtons.OKCancel) = DialogResult.OK Then
            SetInstallPath()
        End If

    End Sub



    Private Sub customChk_CheckedChanged(sender As Object, e As EventArgs) Handles customChk.CheckedChanged
        If customChk.Checked = True Then
            wChk.Enabled = False
            skipChk.Enabled = False
            nsChk.Enabled = False
            dfxChk.Enabled = False
            aspectChk.Enabled = False
            directtxtChk.Enabled = False
        Else
            wChk.Enabled = True
            skipChk.Enabled = True
            nsChk.Enabled = True
            dfxChk.Enabled = True
            aspectChk.Enabled = True
            directtxtChk.Enabled = True
        End If
    End Sub

    Private Sub btnBHPath_Click(sender As Object, e As EventArgs) Handles btnBHPath.Click
        If MsgBox("Current bh path:" & vbCrLf & My.Settings.BHPath & vbCrLf & vbCrLf & "Are you sure you want to change it?", MessageBoxButtons.OKCancel) = DialogResult.OK Then
            SetBHPath()
        End If
    End Sub

    Private Sub SetBHPath()
        Dim FolderBrowserDialog1 As New FolderBrowserDialog

        If FolderBrowserDialog1.ShowDialog() = DialogResult.OK Then
            My.Settings.BHPath = FolderBrowserDialog1.SelectedPath
            My.Settings.BHPath = My.Settings.BHPath.TrimEnd("\") & "\"
            BHPath = My.Settings.BHPath
        End If

        ' DEBUG: MsgBox("Install path set to: " & My.Settings.InstallPath)
        My.Settings.Save()
        My.Settings.Reload()
        Application.Restart()

    End Sub

    Private Sub bhBtn_Click(sender As Object, e As EventArgs) Handles bhBtn.Click
        Try
            Dim bh As New ProcessStartInfo
            bh.FileName = BHPath & "BH.Injector.exe"
            bh.WorkingDirectory = BHPath
            bh.UseShellExecute = True
            bh.Verb = "runas"
            Process.Start(bh)
        Catch ex As Exception
            Dim response = MsgBox("Could not launch BH, did you set the directory for it?", MsgBoxStyle.YesNo)
            If response = MsgBoxResult.Yes Then
                MsgBox("Did you accept the admin privilege request? ¯\_(ツ)_/¯ ")
            Else
                MsgBox("There's the problem, click ok and browse to your BH folder.")
                SetBHPath()
            End If
        End Try
    End Sub

    Private Sub DEP_Only_Click(sender As Object, e As EventArgs) Handles DEP_Only.Click
        Try
            Dim wDEP As New StreamWriter(installPath & "000aaaDEP.bat")
            wDEP.WriteLine("@ECHO OFF")
            wDEP.WriteLine("ECHO %~dp0")
            wDEP.WriteLine("set ""mypath=%~dp0%Diablo II.exe""")
            wDEP.WriteLine("ECHO %mypath%")
            wDEP.WriteLine("ECHO Enabling DEP exception")
            wDEP.WriteLine("call bcdedit.exe /set {current} nx OptOut && color 0A && echo DEP policy changed to allow exceptions || color 0C && echo RUN AS ADMIN")
            wDEP.WriteLine("call REG ADD ""HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\Layers"" /t REG_SZ /d ""~ DisableNXShowUI"" /f /v ""%mypath% ")
            wDEP.WriteLine("PAUSE")
            wDEP.WriteLine("EXIT")
            wDEP.Close()
            MsgBox("When you click OK you will be sent to your Diablo II folder" & vbCrLf & vbCrLf & "Right click on 000aaaDEP.bat and run as admin, the text should come out green, a restart might be required")
            Process.Start(installPath)
        Catch ex As Exception
            MsgBox("Error generating fix file, it's probably open")
        End Try

    End Sub

    Private Sub DEP_and_XP_SP2_Click(sender As Object, e As EventArgs) Handles DEP_and_XP_SP2.Click
        Try
            Dim wDEP As New StreamWriter(installPath & "000aaaDEP+XP_SP2.bat")
            wDEP.WriteLine("@ECHO OFF")
            wDEP.WriteLine("ECHO %~dp0")
            wDEP.WriteLine("set ""mypath=%~dp0%Diablo II.exe""")
            wDEP.WriteLine("ECHO %mypath%")
            wDEP.WriteLine("ECHO Enabling DEP exception")
            wDEP.WriteLine("call bcdedit.exe /set {current} nx OptOut && color 0A && echo DEP policy changed to allow exceptions || color 0C && echo RUN AS ADMIN")
            wDEP.WriteLine("call REG ADD ""HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\Layers"" /t REG_SZ /d ""~ RUNASADMIN WINXPSP2 DisableNXShowUI"" /f /v ""%mypath% ")
            wDEP.WriteLine("PAUSE")
            wDEP.WriteLine("EXIT")
            wDEP.Close()
            MsgBox("When you click OK you will be sent to your Diablo II folder" & vbCrLf & vbCrLf & "Right click on 000aaaDEP+XP_SP2.bat and run as admin, the text should come out green, a restart might be required")
            Process.Start(installPath)
        Catch ex As Exception
            MsgBox("Error generating fix file, it's probably open")
        End Try

    End Sub

    Private Sub ComboBoxLaunchModePicker_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBoxLaunchModePicker.SelectedIndexChanged

    End Sub
End Class
